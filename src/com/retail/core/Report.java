package com.retail.core;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Report {
	
	public void getReport(ItemManager item) {
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").format(new Date());
		System.out.println("\n\n\n");
		System.out.format("%s%50s","****SHIPMENT REPORT****","DATE:"+timeStamp);
		System.out.println("\n\n");
		item.getAllItems();
	}
	
}
