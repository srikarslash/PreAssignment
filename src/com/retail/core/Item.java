package com.retail.core;

class Item implements Comparable<Item> {
	
	private long upc;
	private String description;
	private double price;
	private double weight;
	private ShippingMethod shipping;
	private double shippingCost;

	
	public Item(long upc, String description, double price, double weight, ShippingMethod shipping) {
		super();
		this.upc = upc;
		this.description = description;
		this.price = price;
		this.weight = weight;
		this.shipping = shipping;
		
		this.calShippingCost();
	}
	
	public double getShippingCost() {
		return this.shippingCost;
	}

	public long getUpc() {
		return upc;
	}

	public String getDescription() {
		return description;
	}

	public double getPrice() {
		return price;
	}

	public double getWeight() {
		return weight;
	}

	public ShippingMethod getShipping() {
		return shipping;
	}
	
	private void calShippingCost() {
		if(shipping == ShippingMethod.AIR) {
			//use air
			AirShippingCost cost = new AirShippingCost();
			this.shippingCost = Math.round((cost.calculateShipping(this.weight, this.upc))*100.0)/100.0;
		}else {
			//use ground
			GroundShippingCost cost = new GroundShippingCost();
			this.shippingCost = Math.round((cost.calculateShipping(this.weight, this.upc))*100.0)/100.0;
		}
	}

	public void display() {
		System.out.format("%-30d%-40s%-20.2f%-20.2f%-20s%-20.2f", upc, description, price,weight, shipping,shippingCost);
		System.out.println();
	}


	public int compareTo(Item item) {
		return this.upc > item.upc ? 1 : (this.upc <item.upc ? -1 : 0);
	}


	
	
}



