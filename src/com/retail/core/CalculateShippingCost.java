package com.retail.core;

public interface CalculateShippingCost {
	
	public double calculateShipping(double weight, long upc);	
	
}
