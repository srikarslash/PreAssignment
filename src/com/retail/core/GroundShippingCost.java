package com.retail.core;

public class GroundShippingCost implements CalculateShippingCost {

	@Override
	public  double calculateShipping(double weight, long upc) {
		
		return weight*(2.5);
	}

}
