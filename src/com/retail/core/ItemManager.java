package com.retail.core;

import java.util.ArrayList;
import java.util.Collections;

class ItemManager {
	 

	private static ArrayList<Item> myItems;
	private static double totalShippingCost = 0;
	
	static {
		initialize();
	}
	
	private static void initialize() {
		
		
		myItems = new ArrayList<>();
		myItems.add(new Item(477321101878L, "iPhone -  Headphones", 17.25, 3.21, ShippingMethod.GROUND));
		myItems.add(new Item(567321101987L, "CD � Pink Floyd, Dark Side Of The Moon", 19.99, 0.58, ShippingMethod.AIR));
		myItems.add(new Item(567321101986L, "CD � Beatles, Abbey Road", 17.99, 0.61, ShippingMethod.GROUND));
		myItems.add(new Item(567321101985L, "CD � Queen, A Night at the Opera", 20.49, 0.55, ShippingMethod.AIR));
		myItems.add(new Item(567321101984L, "CD � Michael Jackson, Thriller", 23.88, 0.50, ShippingMethod.GROUND));
		myItems.add(new Item(467321101899L, "iPhone - Waterproof Case", 9.75, 0.73, ShippingMethod.AIR));
		
		Collections.sort(myItems);
		
	}
	
	public void getAllItems(){
		System.out.format("%-40s%-30s%-20s%-20s%-20s%s", "UPC", "Description", "Price","Weight", "Shipping","ShippingCost\n\n");

		for(int i=0;i<myItems.size();i++) {
			myItems.get(i).display();
			
			//calculate total shipping cost 
			double shippingPrice = myItems.get(i).getShippingCost();
			totalShippingCost += shippingPrice;
		}
		
		System.out.println();
		System.out.format("%s%114.2f","TOTAL SHIPPING COST: ",totalShippingCost);
		
		
	}

	
}
